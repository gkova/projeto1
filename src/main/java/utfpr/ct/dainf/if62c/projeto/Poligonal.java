package utfpr.ct.dainf.if62c.projeto;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gabriel
 * @param <T>
 */
public class Poligonal<T extends Ponto> {
//    protected T[] vertices;
    private T[] vertices;

    /**
     *
     * @param v
     */
    public Poligonal(T[] v){
        if(v.length<2)
            throw new RuntimeException("Poligonal deve ter ao menos 2 vértices");
        this.vertices = v;
//        this.vertices = new T[v.length];
//        System.arraycopy(v, 0, vertices, 0, v.length);
    }
    
    public int getN(){
        return vertices.length;
    }
    
    public T get(int verticeN){
        if(verticeN < vertices.length)
            return vertices[verticeN];
        else
            return null;
    }
    
    public void set(int verticeN, T vertice){
        if(verticeN < vertices.length)
            vertices[verticeN] = vertice;
    }
    
    public double getComprimento(){
        double comprimento = 0;
        for(int i=1; i<vertices.length; i++){
            comprimento += vertices[i].dist(vertices[i-1]);
        }
        return comprimento;
    }
}
