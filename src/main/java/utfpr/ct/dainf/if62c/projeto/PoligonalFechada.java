/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.projeto;

/**
 *
 * @author gabriel
 * @param <T>
 */
public class PoligonalFechada<T extends Ponto> extends Poligonal<T>{
    private T[] vertices;/*

    public PoligonalFechada(Ponto2D[] v) {
//        super(v);
        if(v.length<2)
            throw new RuntimeException("Poligonal deve ter ao menos 2 vértices");
        this.vertices = new Ponto2D[v.length];
        System.arraycopy(v, 0, vertices, 0, v.length);
    }
*/    
    
    public PoligonalFechada(T[] v) {
        super(v);
        this.vertices = v;
    }
    @Override
    public double getComprimento(){
        double comprimento = 0;
        for(int i=1; i<vertices.length; i++){
            comprimento += vertices[i].dist(vertices[i-1]);
        }
        comprimento += vertices[0].dist(vertices[vertices.length-1]);
        return comprimento;
    }
}
