package utfpr.ct.dainf.if62c.projeto;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 *
 * @author
 */
public class Ponto {
    // private double x, y, z;
    protected double x, y, z;
    
    public Ponto(){
        x = 0;
        y = 0;
        z = 0;
    }

    /**
     *
     * @param x
     * @param y
     * @param z
     */
    public Ponto(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    /**
     * Retorna o nome não qualificado da classe.
     * @return O nome não qualificado da classe.
     */
    public String getNome() {
        return getClass().getSimpleName();
    }
    
    /**
     *
     * @return
     */
    public double getX(){
        return x;
    }
    
    public double getY(){
        return y;
    }
    
    public double getZ(){
        return z;
    }

    public void setX(double x){
        this.x = x;
    }
    
    public void setY(double y){
        this.y = y;
    }
    
    public void setZ(double z){
        this.z = z;
    }
    
    @Override
    public String toString(){
        String s;
        s = String.format("%s(%f, %f, %f)", this.getNome(), this.x, this.y, this.z);
        return s;
    }
    
    @Override
    public boolean equals(Object obj){
        if(obj == null){
            return false;
        }
        final Ponto q = (Ponto) obj;
        return this.x == q.x && this.y == q.y && this.z == q.z;
    }

   
    
    public double dist(Ponto q){
        return Math.sqrt(Math.pow((x-q.x),2)+Math.pow((y-q.y),2)+Math.pow((z-q.z),2));
    }
}
