/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.projeto;

/**
 *
 * @author gabriel
 */
public class PontoYZ extends Ponto2D{
    
    public PontoYZ()
    {
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }

    /**
     *
     * @param y
     * @param z
     */
    public PontoYZ(double y, double z){
        this.x = 0;
        this.y = y;
        this.z = z;
    }
    
    @Override
    public String toString(){
        String s;
        s = String.format("%s(%f, %f)", this.getNome(), this.y, this.z);
        return s;
    }
}
