
import utfpr.ct.dainf.if62c.projeto.*;


/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author 
 */
public class Projeto1 {

    public static void main(String[] args) {
        
        PontoXZ[] p = new PontoXZ[3];
        p[0] = new PontoXZ(-3,2);
        p[1] = new PontoXZ(-3,6);
        p[2] = new PontoXZ(0,2);
//        System.out.println(p[0].equals(p[1]));
             
        PoligonalFechada triangulo = new PoligonalFechada(p);
        System.out.println("Comprimento da poligonal = " + triangulo.getComprimento());
    }
    
}
